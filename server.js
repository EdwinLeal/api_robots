const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");

const app = express();
app.use(cors());

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

//inicialitzem mongoose
const db = require("mongoose");
db.Promise = global.Promise;

const conn_url = "mongodb://localhost:27017/robots_db";

//connectem a la bdd
db.connect(conn_url, {
    useNewUrlParser: true,
    useUnifiedTopology: true
})
    .then(() => {
        console.log("Connexió OK!");
    })
    .catch(err => {
        console.log("Connexió falla...", err);
        process.exit();
    });

db.set('useFindAndModify', false);

//creem esquema i model 'Robots'
const robotCollection = db.Schema({
    nombre: String,
    fuerza: Number,
    habilidad: Number,
    velocidad: Number,
    partiJ: Number,
    partiG: Number,
    experiencia: Number
});
const Robots = db.model('robot', robotCollection);

//creem esquema i model 'Batallas'
const batallaCollection = db.Schema({ idGanador: String, idPerdedor: String });
const Batallas = db.model('batalla', batallaCollection);


// definim rutes de la API
app.get("/", (req, res) => {
    res.send("<h1>Estais en la API_ROBOTS!</h1>");
});

// consulta TOTS els robots
app.get("/api/robots", (req, res) => {
    Robots.find()
        .then(data => {
            // hacemos cosas con los datos antes de enviar...
            res.status(200).json(data);
        })
        .catch(err => {
            res.status(500).send({
                error: err.message || "Alguna cosa falla."
            });
        });
});

// consulta UN robot per ID
app.get("/api/robots/:id", (req, res) => {
    const id = req.params.id;
    Robots.findById(id)
        .then(data => {
            res.status(200).json(data);
        })
        .catch(err => {
            res.status(500).send({
                error: err.message || "Alguna cosa falla."
            });
        });
});

// crear un nuevo robot
app.post("/api/robots", (req, res) => {
    if (!req.body.nombre) {
        res.status(400).send({ error: "No se encuentra el robot" });
        return;
    }

    // robot nou
    const robot = new Robots({
        nombre: req.body.nombre,
        fuerza: req.body.fuerza,
        habilidad: req.body.habilidad,
        velocidad: req.body.velocidad,
        partiJ: req.body.partiJ,
        partiG: req.body.partiG,
        experiencia: req.body.experiencia,
    });

    // el guardem a la bdd

    robot.save(robot)
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                error: err.message || "Alguna cosa falla."
            });
        });
});

// modifica partidas
app.put("/api/robots/:id", (req, res) => {
    if (!req.body) {
        res.status(400).send({ error: "No trobo dades!" });
        return;
    }

    const id = req.params.id;

    Robots.findByIdAndUpdate(id, req.body)
        .then(data => {
            if (!data) {
                res.send({ error: "No s'ha actualitzat res" });
            } else {
                res.send({ msg: "Actualitzat" });
            }
        })
        .catch(err => {
            res.status(500).send({
                error: err.message || "Alguna cosa falla."
            });
        });
});

// consulta las partidas
app.get("/api/partidas", (req, res) => {
    Batallas.find()
        .then(data => {
            res.status(200).json(data);
        })
        .catch(err => {
            res.status(500).send({
                error: err.message || "Alguna cosa falla."
            });
        });
});

// consulta las estadisticas
app.get("/api/estadisticas/:id", (req, res) => {
    const id = req.params.id;
    Robots.findById(id)
        .then(data => {
            data = { partiJ: data.partiJ, partiG: data.partiG }
            res.status(200).json(data);
        })
        .catch(err => {
            res.status(500).send({
                error: err.message || "Alguna cosa falla."
            });
        });
});


// crear una batalla
app.get("/api/batallas/:id1/:id2", (req, res) => {
    const id1 = req.params.id1;
    const id2 = req.params.id2;
    if (!id1 && !id2) {
        res.status(400).send({ error: "No se encuentra el robot" });
        return;
    }
    let total1;
    let total2;
    let experiencia1;
    let experiencia2;
    let partiJ1;
    let partiG1;
    let partiJ2;
    let partiG2;

    Robots.findById(id1)
        .then(robot1 => {

            let fuerza = robot1.fuerza * 0.4;
            let habilidad = robot1.habilidad * 0.30;
            let velocidad = robot1.velocidad * 0.20;
            experiencia1 = robot1.experiencia * 0.10;
            total1 = fuerza + habilidad + velocidad + experiencia1 * Math.floor(Math.random() * 11)
            partiJ1 = robot1.partiJ
            partiG1 = robot1.partiG

        })
        .then(() => Robots.findById(id2))

        .then(robot2 => {

            let fuerza = robot2.fuerza * 0.4;
            let habilidad = robot2.habilidad * 0.30;
            let velocidad = robot2.velocidad * 0.20;
            experiencia2 = robot2.experiencia * 0.10;
            total2 = fuerza + habilidad + velocidad + experiencia2 * Math.floor(Math.random() * 11)
            partiJ2 = robot2.partiJ
            partiG2 = robot2.partiG

        })
        .then(() => {
            if (total1 > total2) {
                experiencia1 = experiencia1 + 1;
                partiJ1 = partiJ1 + 1;
                partiG1 = partiG1 + 1;
                Robots.findByIdAndUpdate(id1, { experiencia: experiencia1, partiG: partiG1, partiJ: partiJ1 })
                    .then((data) => console.log("modificado", id1, data.nombre, data.partiJ));
                res.status(200).json(id1);

            } else if (total2 > total1) {
                experiencia2 = experiencia2 + 1;
                partiJ2 = partiJ2 + 1;
                partiG2 = partiG2 + 1;
                Robots.findByIdAndUpdate(id2, { experiencia: experiencia2, partiG: partiG2, partiJ: partiJ2 })
                    .then((data) => console.log("modificado", id2, data.nombre, data.partiJ));
                res.status(200).json(id2);
            }
        })
        .then(() => {
            if (total1 > total2) {
                partiJ2 = partiJ2 + 1;
                Robots.findByIdAndUpdate(id2, { partiJ: partiJ2 })
                    .then((data) => console.log("modificado", id1, data.nombre, data.partiJ));


            } else {
                partiJ1 = partiJ1 + 1;
                Robots.findByIdAndUpdate(id1, { partiJ: partiJ1 })
                    .then((data) => console.log("modificado", id1, data.nombre, data.partiJ));
            }
        })
        .catch(err => res.status(500).send({
            error: err.message || "Alguna cosa falla."
        }));

});

//elimina un robot
app.delete("/api/robots/:id", (req, res) => {
    const id = req.params.id;
    Robots.findById(id)
        .then(robot => {
            if (robot.partiJ == 0) {
                Robots.findByIdAndRemove(id)
                    .then(data => {
                        if (!data) {
                            res.status(500).send({ error: "No s'ha eliminat res" });
                        } else {
                            res.send({ msg: "Eliminat", id: id });
                        }

                    })
                    .catch(err => {
                        res.status(500).send({
                            error: err.message || "Alguna cosa falla."
                        });
                    });
            } else {
                res.send({ msg: "No se puede eliminar a un robot que haya luchado" });
            }
        })

});




// set port, listen for requests
const PORT = 8080;
app.listen(PORT, () => {
    console.log(`Server is running on port ${PORT}.`);
});
